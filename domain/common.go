package domain

import (
	"errors"
	"fmt"
	"io"
	"os"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/parnurzeal/gorequest"
	"github.com/spf13/viper"
	lumberjack "gopkg.in/natefinch/lumberjack.v2"
)

var (
	defaultWriter io.Writer
)

// CreateLogFile create a file for logging.
func CreateLogFile(path string) *os.File {
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	return f
}

// DoRequest do request, returns body, statusCode.
func DoRequest(method, url, token string, data string) (string, int, error) {
	defer func() {
		if r := recover(); r != nil {
			log.WithFields(log.Fields{
				"error":  r,
				"url":    url,
				"token":  token,
				"method": method,
				"data":   data,
			}).Error("Http request fail.")
		}
	}()
	log.WithFields(log.Fields{
		"url":    url,
		"token":  token,
		"method": method,
		"data":   data,
	}).Info("Do request.")
	request := gorequest.New().Timeout(10 * time.Second)
	switch method {
	case "GET":
		request = request.Get(url)
	case "POST":
		request = request.Post(url)
	case "PUT":
		request = request.Put(url)
	case "DELETE":
		request = request.Delete(url)
	}
	request = request.Set("Content-Type", "application/json")
	if token != "" {
		auth := fmt.Sprintf("Token %s", token)
		request.Set("Authorization", auth)
	}
	if data != "" {
		request.Send(data)
	}
	rep, body, err := request.End()
	if err != nil {
		log.WithFields(log.Fields{
			"url":         url,
			"token":       token,
			"method":      method,
			"data":        data,
			"status_code": rep.StatusCode,
			"error":       err,
		}).Error("Http request response fail.")
		return "", 0, errors.New("Http request return not right.")
	}
	if rep.StatusCode >= 500 {
		log.WithFields(log.Fields{
			"url":        url,
			"token":      token,
			"method":     method,
			"data":       data,
			"statusCode": rep.StatusCode,
		}).Info("Http request return not right")
		return "", 0, errors.New("Http request return not right.")
	}
	return body, rep.StatusCode, nil
}

//InitLog init log config
func InitLog() {
	ToStdout := viper.GetBool("LOG_TO_STDOUT")
	LogLevel := viper.GetString("LOG_LEVEL")
	LogSize := viper.GetInt("LOG_SIZE")
	customFormatter := new(log.TextFormatter)
	customFormatter.TimestampFormat = "2006-01-02 15:04:05"
	customFormatter.FullTimestamp = true
	log.SetFormatter(customFormatter)

	if ToStdout {
		defaultWriter = os.Stdout
	} else {
		defaultWriter = &lumberjack.Logger{
			Filename:   "/var/log/mathilde/lanaya.log",
			MaxSize:    LogSize, // megabytes
			MaxBackups: 3,
			MaxAge:     28,    //days
			Compress:   false, // disabled by default
		}
	}
	log.SetOutput(defaultWriter)
	switch LogLevel {
	case "INFO":
		log.SetLevel(log.InfoLevel)
	case "WARN":
		log.SetLevel(log.WarnLevel)
	case "ERROR":
		log.SetLevel(log.ErrorLevel)
	default:
		log.SetLevel(log.DebugLevel)
	}
}
