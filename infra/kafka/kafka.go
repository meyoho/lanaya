package kafka

import (
	"strings"
	"time"

	"github.com/Shopify/sarama"
	"github.com/alauda/bergamot/diagnose"
	"github.com/spf13/viper"
)

type Client struct {
	Endpoint string
}

func NewClient() *Client {
	return &Client{
		Endpoint: viper.GetString("KAFKA_HOSTS"),
	}
}

func (c *Client) Diagnose() diagnose.ComponentReport {
	reporter := diagnose.ComponentReport{
		Name:       "kafka",
		Status:     "ok",
		Message:    "",
		Suggestion: "",
		Latency:    0,
	}
	brokerList := strings.Split(c.Endpoint, ",")
	consumerConfig := sarama.NewConfig()
	consumer, err := sarama.NewConsumer(brokerList, consumerConfig)
	begin := time.Now()
	response, err := consumer.Topics()
	reporter.Latency = time.Since(begin)
	if err != nil {
		reporter.Message = err.Error()
		reporter.Status = "error"
		return reporter
	}
	reporter.Message = strings.Join(response, ",")
	return reporter
}
