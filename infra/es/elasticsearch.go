package elasticsearch
import (
	"net/http"
	"fmt"
	"time"
	"io/ioutil"
	"github.com/alauda/bergamot/diagnose"
	"github.com/spf13/viper"
)

type Client struct {
	Endpoint   string
}


func NewClient() *Client {
	return &Client{
		Endpoint:   viper.GetString("ELASTICSEARCH_URL"),
	}
}

func (c *Client) Diagnose() diagnose.ComponentReport {
	reporter := diagnose.ComponentReport{
		Name:       "elasticsearch",
		Status:     "ok",
		Message:    "",
		Suggestion: "",
		Latency:    0,
	}
	url := fmt.Sprintf("%s/_cat/health", c.Endpoint)
	begin := time.Now()
	response, err := http.Get(url)
	reporter.Latency = time.Since(begin)
	if err != nil {
		reporter.Message = err.Error()
		reporter.Status = "error"
		return reporter
	}
	defer response.Body.Close()
    var by []byte
    by,_ = ioutil.ReadAll(response.Body)
	reporter.Message = string(by)
	return reporter
}