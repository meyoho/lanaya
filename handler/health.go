package handler
import (
	"net/http"
	"github.com/gin-gonic/gin"
	"github.com/alauda/bergamot/diagnose"
	"lanaya/infra/es"
	"lanaya/infra/kafka"
)

func PingHandler(c *gin.Context) {
	c.String(http.StatusOK, "lanaya: I don't even question why I question not.!")
}

func DiagnoseHandler(c *gin.Context) {
	reporter, _ := diagnose.New()
	reporter.Add(elasticsearch.NewClient())
	reporter.Add(kafka.NewClient())
	c.JSON(http.StatusOK, reporter.Check())
}