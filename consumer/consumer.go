package consumer

import (
	"bytes"
	"compress/zlib"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"strings"
	"time"

	"github.com/segmentio/kafka-go"
	"github.com/segmentio/kafka-go/sasl/plain"

	log "github.com/Sirupsen/logrus"
	"github.com/olivere/elastic/config"
	"github.com/spf13/viper"
	"gopkg.in/olivere/elastic.v6"
)

// UUIDLength defines the length of UUID.
const UUIDLength int = 40

// LogData is the format of data transferred to ES
type LogData struct {
	CreatedAt    int64             `json:"time"`
	Node         string            `json:"node"`
	PodeName     string            `json:"pod_name"`
	PodID        string            `json:"pod_id"`
	ContainerID  string            `json:"container_id"`
	LogData      string            `json:"log_data"`
	LogType      string            `json:"log_type"`
	LogLevel     int8              `json:"log_level"`
	FileName     string            `json:"file_name"`
	DockerName   string            `json:"docker_container_name"`
	K8sName      string            `json:"kubernetes_container_name"`
	RegionName   string            `json:"region_name"`
	RegionID     string            `json:"region_id"`
	ContainerID8 string            `json:"container_id8"`
	Timestamp    string            `json:"@timestamp"`
	K8sNamespace string            `json:"kubernetes_namespace"`
	RootAccount  string            `json:"root_account"`
	K8sLabels    map[string]string `json:"kubernetes_labels"`
	AppName      string            `json:"application_name"`
	ProjectName  string            `json:"project_name"`
	Path         string            `json:"paths"`
	Source       string            `json:"source"`
	Component    string            `json:"component"`
}

type EventData struct {
	ID           string                 `json:"id"`
	LogLevel     int8                   `json:"log_level"`
	ResourceID   string                 `json:"resource_id"`
	ResourceType string                 `json:"resource_type"`
	Time         int64                  `json:"time,string"`
	Detail       map[string]interface{} `json:"detail"`
}

// MessageHandler wrapper es and kafka client
type MessageHandler struct {
	esClient    *elastic.Client
	esIndexName string
	bulkSize    int32
	kafkaReader *kafka.Reader
}

// CreateConsumer create kafka reader and es writer
func CreateConsumer(kafkaHosts string, kafkaTopicName string, kafkaGroupName string, esURL string, esIndex string) (*MessageHandler, error) {
	var bulkSize int
	var eSUsernameFile = "/etc/pass_es/username"
	var eSPasswordFile = "/etc/pass_es/password"
	var esUsername string
	var esPassword string
	if username, err := ioutil.ReadFile(eSUsernameFile); err == nil {
		esUsername = string(strings.TrimRight(string(username), "\n"))
	}

	if password, err := ioutil.ReadFile(eSPasswordFile); err == nil {
		esPassword = string(strings.TrimRight(string(password), "\n"))
	}
	var kafkaUsernameFile = "/etc/pass_secret/KAFKA_USER"
	var kafkaPasswordFile = "/etc/pass_secret/KAFKA_PASSWORD"
	var kafkaUsername string
	var kafkaPassword string
	if username, err := ioutil.ReadFile(kafkaUsernameFile); err == nil {
		kafkaUsername = string(strings.TrimRight(string(username), "\n"))
	}

	if password, err := ioutil.ReadFile(kafkaPasswordFile); err == nil {
		kafkaPassword = string(strings.TrimRight(string(password), "\n"))
	}

	if kafkaTopicName == viper.GetString("KAFKA_EVENT_TOPICNAME") {
		bulkSize = viper.GetInt("EVENT_BULK_SIZE")
	} else {
		bulkSize = viper.GetInt("BULK_SIZE")
	}
	brokerList := strings.Split(kafkaHosts, ",")
	sniffed := false
	cfg := &config.Config{
		URL:      esURL,
		Username: esUsername,
		Password: esPassword,
		Sniff:    &sniffed,
	}

	esClient, err := elastic.NewClientFromConfig(cfg)
	if err != nil {
		log.Warnf("can't get es client info, please check es cluster for detail... %s", err)
		time.Sleep(time.Duration(5*1000) * time.Millisecond)
		panic(err)
	}
	var reader *kafka.Reader
	if !viper.GetBool("KAFKA_AUTH") {
		reader = kafka.NewReader(kafka.ReaderConfig{
			Brokers:  brokerList,
			GroupID:  kafkaGroupName,
			Topic:    kafkaTopicName,
			MinBytes: 10,   // 10B
			MaxBytes: 10e8, // 1GB
		})
	} else {
		reader = kafka.NewReader(kafka.ReaderConfig{
			Dialer: &kafka.Dialer{
				SASLMechanism: plain.Mechanism{
					Username: kafkaUsername,
					Password: kafkaPassword,
				},
			},
			Brokers:  brokerList,
			GroupID:  kafkaGroupName,
			Topic:    kafkaTopicName,
			MinBytes: 10,   // 10B
			MaxBytes: 10e8, // 1GB
		})
	}

	return &MessageHandler{
		bulkSize:    int32(bulkSize),
		esClient:    esClient,
		esIndexName: esIndex,
		kafkaReader: reader,
	}, nil
}

// CreateLogData ...
func (handler *MessageHandler) CreateLogData(src []byte) ([]LogData, string) {
	logData, token := src[0:len(src)-UUIDLength], src[len(src)-UUIDLength:]
	r := bytes.NewBuffer(logData)
	reader, err := zlib.NewReader(r)
	if err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Error("Create log data fail.")
		return nil, ""
	}
	defer reader.Close()

	var out bytes.Buffer
	io.Copy(&out, reader)
	log := out.String()
	var s []LogData
	json.Unmarshal([]byte(log), &s)
	return s, string(token)
}

func (handler *MessageHandler) createEventData(src []byte) []EventData {
	reader, err := zlib.NewReader(bytes.NewBuffer(src))
	if err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Error("Read event package failed.")
		return nil
	}
	defer reader.Close()

	var (
		eventBuffer bytes.Buffer
		events      []EventData
	)
	io.Copy(&eventBuffer, reader)
	if err := json.Unmarshal(eventBuffer.Bytes(), &events); err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Error("Unmarshal event data failed.")
		return nil
	}
	return events
}

// CreateCommonData ...
func (handler *MessageHandler) CreateCommonData(src []byte) []map[string]interface{} {
	in := bytes.NewBuffer(src)
	event := in.String()
	var s []map[string]interface{}
	json.Unmarshal([]byte(event), &s)
	return s
}

// InsertAuditData ...
func (handler *MessageHandler) InsertAuditData(rows []map[string]interface{}) error {
	log.WithFields(log.Fields{
		"audit info rows": len(rows),
	}).Info("Start insert audit data")
	bulkRequest := handler.esClient.Bulk()
	logNumber := int32(0)
	bulkNumber := 0

	var startTime time.Time
	var insertTime time.Time
	for i := 0; i < len(rows); i++ {
		startTime = time.Now()
		logNumber++
		var auditCreateAt int64
		auditCreateAt = time.Now().Unix() * 1000000
		rows[i]["Timestamp"] = time.Now().UTC().Format("2006-01-02T15:04:05-070000")
		indexNum := fmt.Sprint(auditCreateAt, "-", time.Now().UnixNano())
		// construct the id with AuditID and Stage field, to avoid duplication
		auditID := rows[i]["AuditID"].(string)
		stage := rows[i]["Stage"].(string)
		indexNum = auditID + stage
		esIndex := handler.esIndexName + "-" + handler.GetIndexName(auditCreateAt)
		esType := handler.esIndexName
		newIndex := elastic.NewBulkIndexRequest().Index(esIndex).Type(esType).Id(indexNum).Doc(rows[i])
		bulkRequest = bulkRequest.Add(newIndex)

		if logNumber >= handler.bulkSize {
			insertTime = time.Now()
			_, err := bulkRequest.Do(context.Background())
			if err != nil {
				log.WithFields(log.Fields{
					"error": err,
				}).Error("Audit bulk request fail.")
				panic(err)
				// return err
			}
			endTime := time.Now()
			log.WithFields(log.Fields{
				"bulkSize":   handler.bulkSize,
				"bulkNumber": bulkNumber,
				"costTime":   endTime.Sub(startTime).Seconds(),
				"esCostTime": endTime.Sub(insertTime).Seconds(),
			}).Info("Bulk insert Audit success.")
			logNumber = 0
			bulkNumber++
		}
	}
	insertTime = time.Now()
	if logNumber > 0 {
		log.WithFields(log.Fields{
			"bulkSize":  handler.bulkSize,
			"logNumber": logNumber,
			"rows":      len(rows),
		}).Info("prepare to bulk audit ...")
		_, err := bulkRequest.Do(context.Background())
		if err != nil {
			log.WithFields(log.Fields{
				"error": err,
			}).Error("audit bulk request fail.")
			panic(err)
			// return err
		}
		endTime := time.Now()
		log.WithFields(log.Fields{
			"bulkSize":   handler.bulkSize,
			"bulkNumber": bulkNumber,
			"costTime":   endTime.Sub(startTime).Seconds(),
			"esCostTime": endTime.Sub(insertTime).Seconds(),
			"logNumber":  logNumber,
			"rows":       len(rows),
		}).Info("Audit last package to bulk success.")
	}
	return nil
}

// ConsumeMessage begin consume message and instert into es
func ConsumeMessage(ctx context.Context, KafkaHosts string, KafkaTopicName string, KafkaGroupName string, esURL string, EsIndex string) {
	handler, _ := CreateConsumer(KafkaHosts, KafkaTopicName, KafkaGroupName, esURL, EsIndex)
	insertInterval := viper.GetInt("INSERT_INTERVAL")
	for {
		kafkaCtx, cancel := context.WithTimeout(ctx, 10*time.Second)
		defer cancel()
		select {
		case <-ctx.Done():
			handler.kafkaReader.Close()
			return
		default:
			msg, err := handler.kafkaReader.FetchMessage(kafkaCtx)
			if err != nil {
				log.Warnf("kafka read message with topic/group %v/%v error: %s", KafkaTopicName, KafkaGroupName, err)
				continue
			}
			log.Infof("message at topic/partition/offset %v/%v/%v\n", msg.Topic, msg.Partition, msg.Offset)
			if KafkaTopicName == viper.GetString("KAFKA_EVENT_TOPICNAME") {
				eventData := handler.createEventData(msg.Value)
				err = handler.InsertEventData(eventData)
			} else if KafkaTopicName == viper.GetString("KAFKA_AUDIT_TOPICNAME") {
				auditData := handler.CreateCommonData(msg.Value)
				err = handler.InsertAuditData(auditData)
			} else {
				logData, token := handler.CreateLogData(msg.Value)
				err = handler.InserLogtData(logData, token)
			}
			if err != nil {
				log.Warnf("Insert message into Es error: %s", err)
				continue
			}
			handler.kafkaReader.CommitMessages(ctx, msg)
			time.Sleep(time.Duration(insertInterval) * time.Millisecond)
		}
	}
}

// InserLogtData ...
func (handler *MessageHandler) InserLogtData(rows []LogData, token string) error {
	log.WithFields(log.Fields{
		"rows": len(rows),
	}).Info("Start insert data")
	bulkRequest := handler.esClient.Bulk()
	logNumber := int32(0)
	bulkNumber := 0

	log.WithFields(log.Fields{
		"token":     token,
		"logNumber": len(rows),
	}).Debug("Got token from data. Log list info:")

	var startTime time.Time
	var insertTime time.Time
	for i := 0; i < len(rows); i++ {
		startTime = time.Now()
		if len(rows[i].ContainerID) >= 8 {
			rows[i].ContainerID8 = rows[i].ContainerID[0:8]
		}
		logNumber++
		indexNum := fmt.Sprint(rows[i].CreatedAt, "-", time.Now().UnixNano())
		esIndex := handler.esIndexName + "-" + handler.GetIndexName(rows[i].CreatedAt)
		logType := "log"
		if rows[i].LogType != "file" {
			rows[i].FileName = "stdout"
		}
		if rows[i].Timestamp == "" {
			rows[i].Timestamp = time.Now().UTC().Format("2006-01-02T15:04:05-070000")
		}

		if len(rows[i].LogData) > 10000 {
			rows[i].LogData = rows[i].LogData[0:10000]
		}

		// replace dot in labels with &, for sake of es
		labels := make(map[string]string)
		if viper.GetBool("ELASTICSEARCH_REPLACE_LABEL_DOT") && len(rows[i].K8sLabels) > 0 {
			for k, v := range rows[i].K8sLabels {
				labels[strings.Replace(k, ".", "&", -1)] = v
			}
		}
		// if not ELASTICSEARCH_REPLACE_LABEL_DOT, then labels will be an empty map
		rows[i].K8sLabels = labels

		newIndex := elastic.NewBulkIndexRequest().Index(esIndex).Type(logType).Id(indexNum).Doc(rows[i])
		bulkRequest = bulkRequest.Add(newIndex)

		if logNumber >= handler.bulkSize {
			insertTime = time.Now()
			_, err := bulkRequest.Do(context.Background())
			if err != nil {
				log.WithFields(log.Fields{
					"error": err,
				}).Error("Bulk request fail.")
				panic(err)
				// return err
			}
			endTime := time.Now()
			log.WithFields(log.Fields{
				"bulkSize":   handler.bulkSize,
				"bulkNumber": bulkNumber,
				"costTime":   endTime.Sub(startTime).Seconds(),
				"esCostTime": endTime.Sub(insertTime).Seconds(),
			}).Info("Bulk success.")
			logNumber = 0
			bulkNumber++
		}
	}
	insertTime = time.Now()
	if logNumber > 0 {
		log.WithFields(log.Fields{
			"bulkSize":  handler.bulkSize,
			"logNumber": logNumber,
			"rows":      len(rows),
		}).Info("prepare to bulk ...")
		_, err := bulkRequest.Do(context.Background())
		if err != nil {
			log.WithFields(log.Fields{
				"error": err,
			}).Error("Bulk request fail.")
			panic(err)
			// return err
		}
		endTime := time.Now()
		log.WithFields(log.Fields{
			"bulkSize":   handler.bulkSize,
			"bulkNumber": bulkNumber,
			"costTime":   endTime.Sub(startTime).Seconds(),
			"esCostTime": endTime.Sub(insertTime).Seconds(),
			"logNumber":  logNumber,
			"rows":       len(rows),
		}).Info("Last package to bulk success.")
	}
	return nil
}

// GetIndexName ...
func (handler *MessageHandler) GetIndexName(timestamp int64) string {
	sec := timestamp / 1E6
	nsc := timestamp % 1E6
	nsc = nsc * 1E3
	indexdate := time.Unix(sec, nsc).Format("20060102")
	return indexdate
}

// InsertEventData ...
func (handler *MessageHandler) InsertEventData(rows []EventData) error {
	log.WithFields(log.Fields{
		"event info rows": len(rows),
	}).Info("Start insert event data")
	bulkRequest := handler.esClient.Bulk()
	logNumber := int32(0)
	bulkNumber := 0

	var startTime time.Time
	var insertTime time.Time
	for i := 0; i < len(rows); i++ {
		r := rows[i]
		startTime = time.Now()
		logNumber++
		esIndex := handler.esIndexName + "-" + handler.GetIndexName(r.Time)
		esType := handler.esIndexName
		newIndex := elastic.NewBulkIndexRequest().Index(esIndex).Type(esType).Id(r.ID).Doc(r)
		bulkRequest = bulkRequest.Add(newIndex)

		if logNumber >= handler.bulkSize {
			insertTime = time.Now()
			_, err := bulkRequest.Do(context.Background())
			if err != nil {
				log.WithFields(log.Fields{
					"error": err,
				}).Error("Event bulk request fail.")
				panic(err)
				// return err
			}
			endTime := time.Now()
			log.WithFields(log.Fields{
				"bulkSize":   handler.bulkSize,
				"bulkNumber": bulkNumber,
				"costTime":   endTime.Sub(startTime).Seconds(),
				"esCostTime": endTime.Sub(insertTime).Seconds(),
			}).Info("Bulk insert events success.")
			logNumber = 0
			bulkNumber++
		}
	}
	insertTime = time.Now()
	if logNumber > 0 {
		log.WithFields(log.Fields{
			"bulkSize":  handler.bulkSize,
			"logNumber": logNumber,
			"rows":      len(rows),
		}).Info("prepare to bulk event ...")
		_, err := bulkRequest.Do(context.Background())
		if err != nil {
			log.WithFields(log.Fields{
				"error": err,
			}).Error("event bulk request fail.")
			panic(err)
			// return err
		}
		endTime := time.Now()
		log.WithFields(log.Fields{
			"bulkSize":   handler.bulkSize,
			"bulkNumber": bulkNumber,
			"costTime":   endTime.Sub(startTime).Seconds(),
			"esCostTime": endTime.Sub(insertTime).Seconds(),
			"logNumber":  logNumber,
			"rows":       len(rows),
		}).Info("Event last package to bulk success.")
	}
	return nil
}
