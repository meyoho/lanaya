package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// configCmd represents the config command
var configCmd = &cobra.Command{
	Use:   "config",
	Short: "Print configurations of lanaya.",
	Long:  `Print configurations of lanaya.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("TODO, config called")
	},
}

func init() {
	RootCmd.AddCommand(configCmd)
}
