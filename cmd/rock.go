package cmd

import (
	"context"
	"lanaya/consumer"
	"lanaya/domain"
	"lanaya/handler"

	_ "net/http/pprof"

	"github.com/DeanThompson/ginpprof"
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// rockCmd represents the rock command
var rockCmd = &cobra.Command{
	Use:   "rock",
	Short: "Just rock it",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		KafkaHosts := viper.GetString("KAFKA_HOSTS")
		KafkaTopicName := viper.GetString("KAFKA_TOPIC_NAME")
		KafkaGroupName := viper.GetString("KAFKA_GROUPNAME")
		KafkaEventTopicName := viper.GetString("KAFKA_EVENT_TOPICNAME")
		KafkaEventGroupName := viper.GetString("KAFKA_EVENT_GROUPNAME")
		KafkaAuditTopicName := viper.GetString("KAFKA_AUDIT_TOPICNAME")
		KafkaAuditGroupName := viper.GetString("KAFKA_AUDIT_GROUPNAME")
		EventEsIndex := viper.GetString("ELASTICSEARCH_EVENT_INDEX")
		AuditEsIndex := viper.GetString("ELASTICSEARCH_AUDIT_INDEX")
		EsURL := viper.GetString("ELASTICSEARCH_URL")
		EsIndex := viper.GetString("ELASTICSEARCH_INDEX")
		PartitionNumPerNode := viper.GetInt("PARTITION_NUMBER_PER_NODE")
		domain.InitLog()
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		for i := 0; i < PartitionNumPerNode; i++ {
			go consumer.ConsumeMessage(ctx, KafkaHosts, KafkaTopicName, KafkaGroupName, EsURL, EsIndex)
			go consumer.ConsumeMessage(ctx, KafkaHosts, KafkaEventTopicName, KafkaEventGroupName, EsURL, EventEsIndex)
			go consumer.ConsumeMessage(ctx, KafkaHosts, KafkaAuditTopicName, KafkaAuditGroupName, EsURL, AuditEsIndex)
		}
		router := gin.Default()
		ginpprof.Wrap(router)
		router.GET("/_ping", handler.PingHandler)
		router.GET("/_diagnose", handler.DiagnoseHandler)
		router.GET("/metrics", gin.WrapH(promhttp.Handler()))
		router.Run(":8080")
	},
}

func init() {
	// Adding serve command
	RootCmd.AddCommand(rockCmd)
}
