package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "lanaya",
	Short: "Connect Kafka and Elasticsearch.",
	Long:  `Lanaya read messages from Kafka, in batches, process and bulk-index them into Elasticsearch.`,
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports Persistent Flags, which, if defined here,
	// will be global for your application.

	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.lanaya.yaml)")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" { // enable ability to specify config file via flag
		viper.SetConfigFile(cfgFile)
	}

	viper.SetConfigName(".lanaya") // name of config file (without extension)
	viper.AddConfigPath("$HOME")   // adding home directory as first search path

	viper.SetDefault("KAFKA_HOSTS", "localhost:9092")
	viper.SetDefault("ELASTICSEARCH_URL", "http://localhost:9200")
	viper.SetDefault("ELASTICSEARCH_INDEX", "log")
	viper.SetDefault("ELASTICSEARCH_EVENT_INDEX", "event")
	viper.SetDefault("ELASTICSEARCH_AUDIT_INDEX", "audit")
	viper.SetDefault("ELASTICSEARCH_REPLACE_LABEL_DOT", true)
	viper.SetDefault("PARTITION_TOTAL_NUMBER", 50)
	viper.SetDefault("PARTITION_NUMBER_PER_NODE", 10)
	viper.SetDefault("KAFKA_GROUPNAME", "alauda_log")
	viper.SetDefault("KAFKA_EVENT_GROUPNAME", "alauda_event")
	viper.SetDefault("KAFKA_AUDIT_GROUPNAME", "alauda_audit")
	viper.SetDefault("EVENT_BULK_SIZE", 100)
	viper.SetDefault("CONSUMER_NUMBER", 5)
	viper.SetDefault("BULK_SIZE", 10000)
	viper.SetDefault("KAFKA_TOPIC_NAME", "ALAUDA_LOG_TOPIC")
	viper.SetDefault("KAFKA_EVENT_TOPICNAME", "ALAUDA_EVENT_TOPIC")
	viper.SetDefault("KAFKA_AUDIT_TOPICNAME", "ALAUDA_AUDIT_TOPIC")
	viper.SetDefault("REDIS_TYPE", "normal")
	viper.SetDefault("REDIS_PREFIX", "")
	viper.SetDefault("REDIS_HOST", "localhost:6379")
	viper.SetDefault("REDIS_DB_NAME", 0)
	viper.SetDefault("REDIS_PASSWORD", "")
	viper.SetDefault("REDIS_WRITE_HOST", "")
	viper.SetDefault("REDIS_WRITE_DB_NAME", 0)
	viper.SetDefault("REDIS_WRITE_PASSWORD", "")
	viper.SetDefault("LOG_PATH", "")
	viper.SetDefault("JAKIRO_ENDPOINT", "http://inner-int.alauda.cn")
	viper.SetDefault("JAKIRO_ENDPOINT_VERSION", "v1")
	viper.SetDefault("DRUID_URL", "http://druid-int.alauda.cn:8080")
	viper.SetDefault("DRUID_API_VERSION", "v1")
	viper.SetDefault("LOG_LEVEL", "DEBUG")
	viper.SetDefault("LOG_TO_STDOUT", false)
	viper.SetDefault("LOG_SIZE", 50)
	viper.SetDefault("REDIS_KEY_TIMEOUT", 1200)
	viper.SetDefault("MEMORY_KEY_TIMEOUT", 600)
	viper.SetDefault("DRUID_URL", "")
	viper.SetDefault("INSERT_INTERVAL", 1)
	viper.SetDefault("REDIS_MAX_CONNECTIONS_READER", 32)
	viper.SetDefault("REDIS_MAX_CONNECTIONS_WRITER", 32)
	viper.SetDefault("ALLOW_ALL", false)
	// if VERSION == v2, skip permission check and resource uuid.
	viper.SetDefault("VERSION", "v1")
	viper.SetDefault("KAFKA_AUTH", true)

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
