FROM golang:1.13-alpine as builder
WORKDIR $GOPATH/src/lanaya
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o $GOPATH/src/lanaya/bin/lanaya .
RUN chmod +x $GOPATH/src/lanaya/run.sh

FROM alpine:3.11
COPY --from=builder /go/src/lanaya/bin/lanaya /lanaya/lanaya
COPY --from=builder /go/src/lanaya/run.sh /bin/run.sh
COPY --from=builder /go/src/lanaya/conf/supervisord.conf /etc/supervisord.conf
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories
RUN apk --no-cache --update add bash supervisor upx && \
  mkdir -p /var/log/mathilde
WORKDIR /lanaya
EXPOSE 8080
CMD ["supervisord", "--nodaemon", "--configuration", "/etc/supervisord.conf"]
